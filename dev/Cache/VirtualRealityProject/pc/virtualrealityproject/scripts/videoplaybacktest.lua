local videoplaybacktest = 
{
    Properties = 
    {
		inputSource = {default = EntityId()}
    },
}

function videoplaybacktest:OnActivate()
	--Subscribe to video playback notification handler
	self.videoNotificationHandler = VideoPlaybackNotificationBus.CreateHandler(self, self.entityId)

	self.playPauseNotificationId = GameplayNotificationId(self.Properties.inputSource, "PlayPauseMovie")
	self.stopNotificationId = GameplayNotificationId(self.Properties.inputSource, "StopMovie")

	self.playPause = FloatGameplayNotificationBus.CreateHandler(self, self.playPauseNotificationId)
	self.stop = FloatGameplayNotificationBus.CreateHandler(self, self.stopNotificationId)

	VideoPlaybackRequestBus.Event.EnableLooping(self.entityId, true)
	VideoPlaybackRequestBus.Event.SetPlaybackSpeed(self.entityId, 1.0)
	VideoPlaybackRequestBus.Event.Play(self.entityId)
end

function videoplaybacktest:OnGameplayEventAction(value)
	local busId = self.playPause:GetCurrentBusId()
	local playing = VideoPlaybackRequestBus.Event.IsPlaying(self.entityId)

	if(busId == self.playPauseNotificationId) then
        if(playing) then
		    VideoPlaybackRequestBus.Event.Pause(self.entityId)
		else 
			VideoPlaybackRequestBus.Event.Play(self.entityId)
        end
	elseif(busId == self.stopNotificationId) then
		VideoPlaybackRequestBus.Event.Stop(self.entityId)
	end
end

function videoplaybacktest:OnDeactivate()
	if self.videoNotificationHandler ~= nil then
		self.videoNotificationHandler:Disconnect()
		self.videoNotificationHandler = nil
	end
end

function videoplaybacktest:OnPlaybackStarted()
	Debug.Log("Playback Started")
end

function videoplaybacktest:OnPlaybackPaused()
	Debug.Log("Playback Paused")
end

function videoplaybacktest:OnPlaybackStopped()
	Debug.Log("Playback Stopped")
end

function videoplaybacktest:OnPlaybackFinished()
	Debug.Log("Playback Ended")
end

return videoplaybacktest