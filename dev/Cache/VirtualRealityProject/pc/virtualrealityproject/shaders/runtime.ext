////////////////////////////////////////////////////////////////////////////
//
// All or portions of this file Copyright (c) Amazon.com, Inc. or its affiliates or
// its licensors.
//
// For complete copyright and license terms please see the LICENSE at the root of this
// distribution (the "License"). All use of this software is governed by the License,
// or, if provided, by the license below or the license accompanying this file. Do not
// remove or modify any license notices. This file is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//
// Original file Copyright Crytek GMBH or its affiliates, used under license.
//

//  Description:
//
////////////////////////////////////////////////////////////////////////////

Version (1.00)

Property
{
  Name = %_RT_FOG
  Mask = 0x1
  Precache = GeneralPS
  Precache = GeneralVS
  Precache = GeneralGS
  Precache = GeneralDS
  Precache = GeneralHS
  Precache = TerrainPS
  Precache = TerrainVS
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
  Precache = SkinPS
  Precache = SkinVS
  Precache = HairPS
  Precache = HairVS  
  Precache = EyePS
  Precache = EyeVS  
  Precache = GlassPS
  Precache = GlassVS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS 
  Precache = CustomRenderHS
  Precache = CustomRenderDS
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS
  Precache = WaterFogVolume_VS
  Precache = WaterFogVolume_PS
  Precache = MeshBakerPS
	Precache = ParticleImposterVS
	Precache = ParticleImposterPS
}

Property
{
  Name = %_RT_AMBIENT
  Mask = 0x2
  Precache = GeneralPS
  Precache = SkinPS
  Precache = HairPS  
  Precache = EyePS
  Precache = GlassPS
  Precache = TerrainPS 
  Precache = VegetationPS
  Precache = ParticlePS
  Precache = MeshBakerPS
}

Property
{
  Name = %_RT_OCEAN_PARTICLE
  Mask = 0x4
  Precache = PostProcessGamePS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS

  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS
  Precache = WaterFogVolume_PS
}

Property
{
  Name = %_RT_DECAL_TEXGEN_2D
  Mask = 0x8
  Precache = GeneralVS
  Precache = GeneralDS
  Precache = GeneralHS
  Precache = ShadowGenVS  
  Precache = CausticsVS
  Precache = GeneralPS
  Precache = CustomRenderHS
  Precache = CustomRenderDS
  Precache = ZVS
  Precache = ZPS
  Precache = MeshBakerPS
  Precache = TiledShadingCS
}

Property
{
  Name = %_RT_DISSOLVE
  Mask = 0x10
  Precache = ZPS
  Precache = ZVS
  Precache = ShadowGenVS
  Precache = ShadowGenPS
}

Property
{
  Name = %_RT_VOLUMETRIC_FOG
  Mask = 0x20
  Precache = GeneralPS
  Precache = GeneralVS
  Precache = GeneralGS
  Precache = GeneralDS
  Precache = GeneralHS
  Precache = TerrainPS
  Precache = TerrainVS
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
  Precache = SkinPS
  Precache = SkinVS
  Precache = HairPS
  Precache = HairVS
  Precache = EyePS
  Precache = EyeVS
  Precache = GlassPS
  Precache = GlassVS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
  Precache = WaterSurfacePS
  Precache = WaterFogVolume_VS
  Precache = WaterFogVolume_PS
  Precache = MeshBakerPS
  Precache = CloudVS
  Precache = CloudPS
  Precache = FogPostProcessPS
  Precache = ParticleImposterVS
  Precache = ParticleImposterPS
}

Property
{
  Name = %_RT_NEAREST
  Mask = 0x40
  Precache = ShadowGenVS
  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS
  Precache = ZVS
  Precache = MotionBlurVS
  Precache = SkinVS
  Precache = GeneralVS
  Precache = VegetationVS    
  Precache = CausticsVS
  Precache = CustomRenderVS
  Precache = DebugPassVS
}

Property
{
  Name = %_RT_GLOBAL_ILLUMINATION
  Mask = 0x80
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
}

Property
{
  Name = %_RT_ALPHATEST
  Mask = 0x100
  Precache = ShadowGenVS
  Precache = ShadowGenPS
  Precache = ZPS
  Precache = ZVS
  Precache = MotionBlurPS
  Precache = CustomRenderPS
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
	Precache = GeneralPS
  Precache = GlassPS
  Precache = GlassVS
  Precache = MotionBlurVS
  Precache = MeshBakerPS
}

Property
{
  Name = %_RT_SOFT_PARTICLE
  Mask = 0x200
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS  
}

Property
{
  Name = %_RT_HDR_MODE
  Mask = 0x400
  Precache = GeneralPS
  Precache = TerrainPS
  Precache = VegetationPS
  Precache = SkinPS
  Precache = HairPS
  Precache = EyePS
  Precache = GlassPS
  Precache = ParticlePS
  Precache = WaterSurfacePS
	Precache = ParticleImposterPS
}

Property
{
  Name = %_RT_PARTICLE_SHADOW
  Mask = 0x800
  Precache = ParticlePS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
}

Property
{
  Name = %_RT_SAMPLE1
  Mask = 0x1000
  Precache = CustomRenderHS
  Precache = CustomRenderDS
  Precache = CustomRenderVS
  Precache = CustomRenderPS
  Precache = SkinPS
  Precache = HDRPostProcessPS
  Precache = PostMotionBlurVS
  Precache = PostMotionBlurPS
  Precache = PostSunShaftsPS
  Precache = PostProcessGamePS
  Precache = PostDofPS
  Precache = PostEffectsVS
  Precache = PostEffectsPS
  Precache = PostAA_PS
  Precache = DeferredDecalPassPS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = PostHUD3D_VS
  Precache = PostHUD3D_PS
  Precache = ParticlePS 
  Precache = ParticleVS
  Precache = DeferredRainPS
	Precache = ShadowMaskGenPS

  Precache = WaterFogVolume_VS
  Precache = WaterFogVolume_PS
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS

	Precache = BeamPS
  Precache = ParticleHS
  Precache = ParticleDS
	
	Precache = TiledShadingCS
	Precache = VolumeLightInjectionCS
	Precache = ResolvePS
}

Property
{
  Name = %_RT_SAMPLE2
  Mask = 0x2000
  
  Precache = HDRPostProcessPS
  Precache = PostMotionBlurVS
  Precache = PostMotionBlurPS
  Precache = PostAA_PS
  Precache = PostSunShaftsPS
  Precache = PostProcessGamePS
  Precache = DeferredDecalPassPS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = CustomRenderPS
  Precache = DeferredRainPS

  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS 
  Precache = ParticlePS 
  Precache = ParticleVS 
  Precache = ParticleHS
  Precache = ParticleDS

	Precache = BeamPS
	Precache = LensOpticsPS

  Precache = WaterFogVolume_PS
	
	Precache = TiledShadingCS
  Precache = FogPostProcessPS
}

Property
{
  Name = %_RT_SAMPLE3
  Mask = 0x4000
  Precache = ParticlePS 
  Precache = ParticleVS 
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = PostAA_PS
  Precache = HDRPostProcessPS
  Precache = BeamPS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = DeferredRainPS
  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS

	Precache = LensOpticsPS
  Precache = WaterFogVolume_PS
	
	Precache = TiledShadingCS
}

Property
{
  Name = %_RT_POINT_LIGHT
  Mask = 0x8000
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = WaterFogVolume_PS
  Precache = ConeTraceDiffusePS
}
	
Property
{
  Name = %_RT_ALPHABLEND
  Mask = 0x10000
  Precache = GeneralHS
  Precache = GeneralDS
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = ParticlePS
  Precache = ParticleVS
	Precache = HairVS
  Precache = EyeVS
  Precache = VegetationVS
  Precache = GeneralVS 
  Precache = GlassVS
  Precache = CustomRenderHS
  Precache = CustomRenderDS
  Precache = ZVS
  Precache = ZPS
  Precache = MotionBlurPS
}

Property
{
  Name = %_RT_ANIM_BLEND
  Mask = 0x20000
  Precache = ParticlePS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticleGS
}

Property
{
  Name = %_RT_QUALITY
  Mask = 0x40000
  AutoPrecache
  Precache = ShadowGenVS
  Precache = ShadowGenPS
  Precache = ZVS
  Precache = ZPS
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = GeneralPS
  Precache = GeneralVS
  Precache = SkinPS
	Precache = SkinVS 
	Precache = HairPS
	Precache = HairVS 
  Precache = EyePS
  Precache = EyeVS
  Precache = GlassPS
  Precache = GlassVS
  Precache = TerrainPS  
  Precache = TerrainVS  
  Precache = VegetationPS  
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationVS  
  Precache = MotionBlurVS  
  Precache = MotionBlurPS  
  Precache = CausticsVS
  Precache = ParticlePS
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  
  Precache = PostMotionBlurVS
  Precache = PostMotionBlurPS
  Precache = PostSunShaftsPS
  Precache = SpriteDilatePS
  Precache = ShadowMaskGenPS
  
  Precache = HDRPostProcessVS
  Precache = HDRPostProcessPS
  Precache = FogPostProcessPS
  Precache = PostProcessGameVS
  Precache = PostProcessGamePS

  Precache = DistanceCloudsPS

	Precache = DeferredLightPassPS

	Precache = VolumeLightInjectionCS
}

Property
{
  Name = %_RT_QUALITY1
  Mask = 0x80000
  AutoPrecache
  Precache = ShadowGenVS
  Precache = ShadowGenPS  
  Precache = ZVS
  Precache = ZPS
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = GeneralPS
  Precache = GeneralVS
  Precache = SkinPS
	Precache = SkinVS
	Precache = HairPS
	Precache = HairVS
  Precache = EyePS
  Precache = EyeVS
  Precache = GlassPS
  Precache = GlassVS
  Precache = TerrainPS  
  Precache = TerrainVS  
  Precache = VegetationPS  
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationVS  
  Precache = MotionBlurVS  
  Precache = MotionBlurPS  
  Precache = CausticsVS
  Precache = ParticlePS
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS 
  
  Precache = PostMotionBlurVS
  Precache = PostMotionBlurPS
  Precache = PostSunShaftsPS
  Precache = SpriteDilatePS
  Precache = ShadowMaskGenPS
  
  Precache = HDRPostProcessVS
  Precache = HDRPostProcessPS
  Precache = FogPostProcessPS
  Precache = PostProcessGameVS
  Precache = PostProcessGamePS

  Precache = DistanceCloudsPS

	Precache = DeferredLightPassPS

	Precache = VolumeLightInjectionCS
}

Property
{
  Name = %_RT_INSTANCING_ATTR
  Mask = 0x100000
  Precache = GeneralVS
	Precache = SkinVS 	
	Precache = HairVS 
  Precache = EyeVS
  Precache = GlassVS
  Precache = VegetationVS    
  Precache = ShadowGenVS  
  Precache = ZVS
  Precache = MotionBlurVS
  Precache = CausticsVS
  Precache = CustomRenderVS
	Precache = DebugPassVS
}

Property
{
  Name = %_RT_ENVIRONMENT_CUBEMAP
  Mask = 0x200000
  Precache = ParticlePS
}

Property
{
  Name = %_RT_TILED_SHADING
  Mask = 0x400000
	Precache = EyePS
	Precache = HairPS
	Precache = GlassPS
}

Property
{
  Name = %_RT_NO_TESSELLATION
  Mask = 0x800000
  Precache = GeneralVS
  Precache = GeneralPS
  Precache = ShadowGenVS  
  Precache = ShadowGenPS  
  Precache = ZVS
  Precache = ZPS
  Precache = MotionBlurVS
  Precache = MotionBlurPS
  Precache = CustomRenderVS
  Precache = SkinVS
  Precache = SkinPS
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
  
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
  Precache = DebugPassVS
}

Property
{
  Name = %_RT_AMBIENT_OCCLUSION
  Mask = 0x1000000
  // hack - disable caching
  Precache = TerrainPS // vlad: reguired for terrain base normal mapping
  Precache = TerrainVS  
}

Property
{
  Name = %_RT_LIGHT_TEX_PROJ
  Mask = 0x2000000
  Precache = DeferredLightPassPS
	
  Precache = ParticleVS
  Precache = ParticleDS
  Precache = ConeTraceDiffusePS
}

Property
{
  Name = %_RT_VERTEX_VELOCITY
  Mask = 0x4000000
  Precache = ZVS
	Precache = MotionBlurVS	
}

Property
{
  Name = %_RT_SKELETON_SSD
  Mask = 0x8000000
  Precache = GeneralVS
	Precache = SkinVS 
	Precache = HairVS	
  Precache = EyeVS
  Precache = GlassVS
  Precache = VegetationVS    
  Precache = ShadowGenVS
  Precache = ZVS
  Precache = MotionBlurVS
  Precache = CausticsVS
  Precache = CustomRenderVS
	Precache = DebugPassVS
}

Property
{
  Name = %_RT_SKELETON_SSD_LINEAR
  Mask = 0x10000000
  Precache = GeneralVS
	Precache = SkinVS 
	Precache = HairVS	
  Precache = EyeVS
  Precache = GlassVS
  Precache = VegetationVS    
  Precache = ShadowGenVS
  Precache = ZVS
  Precache = MotionBlurVS
  Precache = CausticsVS
  Precache = CustomRenderVS
	Precache = DebugPassVS
}

Property
{
  Name = %_RT_BLEND_WITH_TERRAIN_COLOR
  Mask = 0x20000000
	Precache = ZVS
	Precache = ZPS
	Precache = GeneralHS
	Precache = GeneralDS
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
  Precache = ConeTraceDiffusePS
}

Property
{
  Name = %_RT_MOTION_BLUR
  Mask = 0x40000000
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = GPUParticleCS
  Precache = ParticleDS
  Precache = ParticlePS
	Precache = ZVS
	Precache = ZGS
	Precache = ZPS
	Precache = GeneralHS
	Precache = GeneralDS
}

Property
{
  Name = %_RT_LIGHTVOLUME0
  Mask = 0x80000000
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = VolumeLightInjectionCS
  Precache = RenderDownscaledShadowMapPS
}

Property
{
  Name = %_RT_LIGHTVOLUME1
  Mask = 0x100000000
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
  Precache = VolumeLightInjectionCS
  Precache = RenderDownscaledShadowMapPS
}

Property
{
  Name = %_RT_NOZPASS
  Mask = 0x200000000
  Precache = VegetationVS
  Precache = VegetationHS
  Precache = VegetationDS
  Precache = VegetationPS
}

Property
{
  Name = %_RT_SHADOW_MIXED_MAP_G16R16
  Mask = 0x400000000
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = WaterFogVolume_PS

  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS
}

Property
{
  Name = %_RT_SHADOW_JITTERING
  Mask = 0x800000000
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = TerrainPS
  Precache = WaterFogVolume_PS
  
  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS
	
	Precache = HairPS
	Precache = EyePS
}

Property
{
  Name = %_RT_HDR_ENCODE
  Mask = 0x1000000000
  //Precache = GeneralPS
  Precache = TerrainPS
  Precache = VegetationPS
  
  Precache = FogPostProcessPS
}

Property
{
  Name = %_RT_SAMPLE0
  Mask = 0x2000000000
  Precache = CustomRenderVS
  Precache = CustomRenderPS
  Precache = SkinPS
  Precache = GlassPS

  Precache = FogPostProcessPS
  Precache = HDRPostProcessPS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
  Precache = ParticlePS
  Precache = PostHUD3D_VS
  Precache = PostHUD3D_PS
  Precache = PostMotionBlurVS
  Precache = PostMotionBlurPS
  Precache = PostAA_PS
  Precache = PostSunShaftsPS
  Precache = PostProcessGameVS
  Precache = PostProcessGamePS
  Precache = PostDofPS
  Precache = PostEffectsVS
  Precache = PostEffectsPS
  Precache = DeferredDecalPassPS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = SceneRainVS
  Precache = SceneRainPS
  Precache = DeferredRainPS
  Precache = ResolveVS
  Precache = ResolvePS
  
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS
  Precache = WaterFogVolume_PS

	Precache = BeamPS
	Precache = LensOpticsVS

	Precache = TiledShadingCS
	Precache = VolumeLightInjectionCS
}

// Reserved for post processes/deferred - do not use for light/common shaders
Property
{
  Name = %_RT_SAMPLE5
  Mask = 0x4000000000
  
  Precache = ZVS
  Precache = GeneralVS
  Precache = GeneralHS
  Precache = GeneralDS
  
  Precache = HDRPostProcessPS
  Precache = PostSunShaftsPS
  Precache = PostProcessGamePS
  Precache = DeferredPassPS
	Precache = DeferredPassVS
	Precache = DeferredLightPassPS
	Precache = PostMotionBlurPS
  Precache = DeferredDecalPassPS
  Precache = ResolvePS
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = CustomRenderHS
  Precache = CustomRenderDS
  Precache = CustomRenderPS
  Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS
  Precache = WaterFogVolume_PS

	Precache = BeamPS
	Precache = LensOpticsVS
	
	Precache = TiledShadingCS

	Precache = VolumeLightInjectionCS
	Precache = ReprojectVolumetricFogCS
}

Property
{
  Name = %_RT_HW_PCF_COMPARE
  Mask = 0x8000000000
  Precache = FogPassVolShadowsInterleavePassPS
  Precache = ShadowGenVS
  Precache = ShadowGenPS
  
  Precache = DeferredLightPassPS
  Precache = ShadowMaskGenVS
  Precache = ShadowMaskGenPS

  Precache = WaterFogVolume_PS
  Precache = ConeTraceDiffusePS
}

Property
{
  Name = %_RT_REVERSE_DEPTH
	Mask = 0x10000000000
	Precache = DistanceCloudsVS
	Precache = PostProcessGamePS
	Precache = TerrainVS
	Precache = ZVS
	Precache = ZPS
	Precache = UnderwaterGodRays
	Precache = WaterSurfaceVS
	Precache = WaterFogVolume_VS
	Precache = GeneralVS
	Precache = LensOpticsVS
}

Property
{
  Name = %_RT_DEBUG0
  Mask = 0x20000000000
  
  Runtime
}

Property
{
  Name = %_RT_DEBUG1
  Mask = 0x40000000000
  Runtime
}

Property
{
  Name = %_RT_DEBUG2
  Mask = 0x80000000000
  Runtime
}

Property
{
  Name = %_RT_DEBUG3
  Mask = 0x100000000000  
  Runtime
}

Property
{
  Name = %_RT_CUBEMAP0
  Mask = 0x200000000000 
  Precache = ShadowGenPS
  Precache = ShadowGenVS
  
  Precache = PostSunShaftsPS
  Precache = PostProcessGamePS
  
  Precache = DeferredDecalPassVS
  Precache = DeferredDecalPassPS
  Precache = DeferredLightPassVS
  Precache = DeferredLightPassPS
  Precache = DeferredPassVS
  Precache = DeferredPassPS
  
  Precache = WaterFogVolume_PS
  Precache = TiledShadingCS
}

Property
{
  Name = %_RT_SAMPLE4
  Mask = 0x400000000000 
  Precache = ShadowGenVS
  Precache = ShadowGenPS
	Precache = PostAA_PS
  
  Precache = HDRPostProcessPS
  Precache = PostProcessGamePS
  Precache = DeferredLightPassPS
  Precache = DeferredPassPS
  Precache = PostSunShaftsPS
  Precache = PostMotionBlurPS

	Precache = WaterSurfaceVS
  Precache = WaterSurfacePS
  Precache = WaterSurfaceHS
  Precache = WaterSurfaceDS
  Precache = WaterFogVolume_PS
	Precache = DeferredDecalPassPS

  Precache = BeamPS
  Precache = LensOpticsPS
	
	Precache = TiledShadingCS

  Precache = ConeTraceDiffusePS
  Precache = VolumeLightInjectionCS
}

Property
{
  Name = %_RT_SPRITE
  Mask = 0x800000000000 
  Precache = VegetationVS
  Precache = VegetationPS
  Precache = ParticleVS
  Precache = ParticleHS
  Precache = ParticleDS
}

Property
{
    Name = %_RT_GPU_PARTICLE_SHADOW_PASS
    Mask = 0x1000000000000  // 1 << 50
    Precache = ParticleVS
    Precache = ParticlePS
}

Property
{
    Name = %_RT_GPU_PARTICLE_DEPTH_COLLISION
    Mask = 0x2000000000000 // 1 << 57
    Precache = GPUParticleCS
}


Property
{
    Name = %_RT_GPU_PARTICLE_TURBULENCE
    Mask = 0x4000000000000 // 1 << 58
    Precache = GPUParticleCS
}


Property
{
    Name = %_RT_GPU_PARTICLE_UV_ANIMATION
    Mask = 0x8000000000000 // 1 << 59
    Precache = ParticleVS
    Precache = ParticlePS
}

Property
{
    Name = %_RT_GPU_PARTICLE_NORMAL_MAP
    Mask = 0x10000000000000
    Precache = ParticleVS
    Precache = ParticlePS
}

Property
{
    Name = %_RT_GPU_PARTICLE_GLOW_MAP
    Mask = 0x20000000000000
    Precache = ParticleVS
    Precache = ParticlePS
}

Property
{
    Name = %_RT_GPU_PARTICLE_CUBEMAP_DEPTH_COLLISION
    Mask = 0x40000000000000
    Precache = GPUParticleCS
}

Property
{
    Name = %_RT_GPU_PARTICLE_WRITEBACK_DEATH_LOCATIONS
    Mask = 0x80000000000000
    Precache = GPUParticleCS
}

Property
{
    Name = %_RT_GPU_PARTICLE_TARGET_ATTRACTION
    Mask = 0x100000000000000
    Precache = GPUParticleCS
}

Property
{
    Name = %_RT_GPU_PARTICLE_SHAPE_ANGLE
    Mask = 0x200000000000000
    Precache = GPUParticleCS
}

Property
{
    Name = %_RT_GPU_PARTICLE_SHAPE_BOX
    Mask = 0x400000000000000
    Precache = GPUParticleCS
    Precache = ParticleVS
}


Property
{
    Name = %_RT_GPU_PARTICLE_SHAPE_POINT
    Mask = 0x800000000000000
    Precache = GPUParticleCS
}

Property
{
    Name = %_RT_GPU_PARTICLE_SHAPE_CIRCLE
    Mask = 0x1000000000000000
    Precache = GPUParticleCS
    Precache = ParticleVS
}

Property
{
    Name = %_RT_GPU_PARTICLE_SHAPE_SPHERE
    Mask = 0x2000000000000000
    Precache = GPUParticleCS
    Precache = ParticleVS
}

Property
{
    Name = %_RT_GPU_PARTICLE_WIND
    Mask = 0x4000000000000000
    Precache = GPUParticleCS

}