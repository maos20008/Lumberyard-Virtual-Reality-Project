hidden = {
	Properties = 
	{
		Hide = true;
	}
}

function hidden:OnActivate()
	-- there's a bug that preventing SetVisibility from working during activate,
	-- so subscribe to TickBus and call it next frame
	if self.Properties.Hide then
		self.tickHandler = TickBus.Connect(self)
	end
end

function hidden:OnTick(dt, time)
	MeshComponentRequestBus.Event.SetVisibility(self.entityId, false)
	--self.tickHandler:Disconnect()
end

return hidden