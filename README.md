# Lumberyard Virtual Reality Project(Unofficial User Guide)
![amazon-lumberyard](https://user-images.githubusercontent.com/18353476/28592053-ae35f67a-713c-11e7-9d2b-9fab8de0c94c.jpg)

Install Lumberyard: https://aws.amazon.com/lumberyard/

Lumberyard Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/setting-up-system-requirements.html

Amazon GameDev Tutorials: https://gamedev.amazon.com/forums/tutorials

An Introduction to AWS Lumberyard Game Development: https://www.packtpub.com/books/content/introduction-aws-lumberyard-game-development

VR Islands Level Sample: https://docs.aws.amazon.com/lumberyard/latest/userguide/sample-level-vr-islands.html

# Oculus Rift Health and Safety Warning Guide
https://www.oculus.com/legal/health-and-safety-warnings/

# HTC Vive Safety and Regulatory Guide
http://dl4.htc.com/web_materials/Safety_Guide/Vive/Vive_safety_and_regulatory_guide_ENG-FRC-ESM.pdf

# Lumberyard Support for macOS(Preview release)

macOS Support Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/osx-intro.html

# Lumberyard Support for Linux(Preview release)

https://docs.aws.amazon.com/lumberyard/latest/userguide/linux-intro.html

# System Requirements

Lumberyard requires the following hardware and software:

Windows 7 64-bit or Later

3GHz minimum quad-core processor

8 GB RAM minimum

2 GB minimum DX11 or later compatible video card

Nvidia driver version 368.81 or AMD driver version 16.15.2211 graphics card

60 GB minimum of free disk space

One or both of the following: (required to compile Lumberyard Editor and tools)

    Visual Studio 2013 Update 4 or later
    
    Visual Studio 2015 Update 3 or later
    
 # Setup and Configure Projects in Lumberyard
 
![1_launcher](https://user-images.githubusercontent.com/18353476/28295300-864421c4-6b14-11e7-8771-40a598086499.png)
![2_setupgems](https://user-images.githubusercontent.com/18353476/28333786-f2a032d8-6bad-11e7-97db-59fd6675c37d.png)
![vr](https://user-images.githubusercontent.com/18353476/28665615-0b1322a4-7279-11e7-813f-b59a3b1b5d50.PNG)
![vr-samples-project-2](https://user-images.githubusercontent.com/18353476/28296794-32c3452e-6b1f-11e7-902f-72e6b9008ddf.jpg)

# Sample Demos
![vr-sample-xylophone 1](https://user-images.githubusercontent.com/18353476/28398008-ed87d2e4-6cb9-11e7-897a-6792253c4008.gif)
![vr-sample-box-garden](https://user-images.githubusercontent.com/18353476/28398010-ef801638-6cb9-11e7-8083-a02e9c5e4b54.gif)
![vr-sample-many-tvs](https://user-images.githubusercontent.com/18353476/28646667-cefb127e-7217-11e7-84fd-4ecbb451dd1f.gif)
